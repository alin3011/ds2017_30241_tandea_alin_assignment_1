package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;

import org.jdom2.JDOMException;
import org.xml.sax.SAXException;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class TimeUtils {
	public String getLocalTime(String city)
			throws IOException, SAXException, ParserConfigurationException, JDOMException {
		String urlString = "http://www.datasciencetoolkit.org/maps/api/geocode/json?sensor=false" + "&address=";
		URL url = new URL(urlString + city);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		String output;
		String lng = null;
		String lat = null;
		while ((output = br.readLine()) != null) {
			if (output.contains("lng")) {
				lng = output.substring(17, 21);
			}
			if (output.contains("lat")) {
				lat = output.substring(17, 21);
				break;
			}
		}
		;
		conn.disconnect();

		urlString = "http://api.geonames.org/timezoneJSON?formatted=true&";
		urlString += "lat=" + lat;
		urlString += "&lng=" + lng + "&username=alindan&style=full";
		System.out.println(urlString);
		url = new URL(urlString + city);
		conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/xml");
		br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		StringBuilder sb = new StringBuilder();
		while ((output = br.readLine()) != null) {
			sb.append(output);
		}
		System.out.println(sb.toString());
		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(sb.toString()).getAsJsonObject();
		String time = obj.get("time").getAsString();
		return time;
	}
}

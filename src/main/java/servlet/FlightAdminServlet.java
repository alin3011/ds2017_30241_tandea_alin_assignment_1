package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import access.FlightDAO;
import model.Flight;

/**
 * Servlet implementation class FlightAdminServlet
 */
public class FlightAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	FlightDAO f = new FlightDAO();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FlightAdminServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer role = (Integer) session.getAttribute("role");
		if (role != null && role.intValue() == 1) {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			List<Flight> flights = f.findAll();
			out.print("<html><head><title></title></head><body>");

			out.println("<br /><form action = \"handle\" method = \"POST\">"
					+ "Flight number:\t<input type = \"text\" name = \"idFlight\">"
					+ "<br />Airplane type:\t<input type = \"text\" name = \"airplaneType\">"
					+ "<br />Departure city:\t<input type = \"text\" name = \"departureCity\">"
					+ "<br />Departure time:\t<input type = \"text\" name = \"departureTime\">"
					+ "<br />Arrival city:\t<input type = \"text\" name = \"arrivalCity\">"
					+ "<br />Arrival time:\t<input type = \"text\" name = \"arrivalTime\">"
					+ "<br /><input type = \"submit\" value = \"Edit flight\" />"
					+ "<input type = \"submit\" value = \"Add flight\" />" + "</form>"

					+ "<br /><form action = \"handle\" method = \"GET\">"
					+ "Flight number: <input type = \"text\" name = \"idFlight\">"
					+ "<input type = \"submit\" value = \"Delete flight\" />" + "</form>");

			out.print("<table><tr>" + "<th>Flight number</th>" + "<th>Airplane type</th>" + "<th>Departure city</th>"
					+ "<th>Departure time</th>" + "<th>Arrival city</th>" + "<th>Arrival time</th>" + "</tr>");
			for (Flight flight : flights) {
				try {
					out.print("<tr>");
					out.print("" + "<td>" + flight.getIdFlight() + "</td>" + "<td>" + flight.getAirplaneType() + "</td>"
							+ "<td>" + flight.getDepartureCity() + "</td>" + "<td>"
							+ flight.getDepartureTime().toString() + "</td>" + "<td>" + flight.getArrivalCity()
							+ "</td>" + "<td>" + flight.getArrivalTime().toString() + "</td>" + "");
					out.print("</tr>");
				} catch (Exception e) {

				}
			}
			out.println("</table>");
			out.print("</table>");
			out.println("<form action = \"LoginServlet\" method = \"GET\">"
					+ "<input type = \"submit\" value = \"Logout\" />" + "</form>");
			out.print("</body></html>");

			out.print("</body></html>");
		} else {
			response.sendError(403, "forbidden");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}

package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import access.FlightDAO;
import model.Flight;
import utils.TimeUtils;

public class FlightServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 774591386733233668L;

	FlightDAO f = new FlightDAO();
	TimeUtils timeUtils = new TimeUtils();

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {

		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		List<Flight> flights = f.findAll();
		try {
			String idFlight = req.getParameter("idFlight");
			Flight flight = f.getFlight(Integer.parseInt(idFlight));
			System.out.println(flight.getIdFlight());
			System.out.println(timeUtils.getLocalTime(flight.getDepartureCity()));
			out.println("Local departure time for " + flight.getDepartureCity()
					+ timeUtils.getLocalTime(flight.getDepartureCity()) + "<br />");
			out.println("Local arrival time for " + flight.getArrivalCity()
					+ timeUtils.getLocalTime(flight.getArrivalCity()) + "<br />");
		} catch (Exception e) {
			// e.printStackTrace();
		}
		out.print("<html><head><title></title></head><body><table>");
		out.println("<form action = \"display\" method = \"GET\">"
				+ "Flight number: <input type = \"text\" name = \"idFlight\">"
				+ "<input type = \"submit\" value = \"Get local time\" />" + "</form>");

		out.print("<tr>" + "<th>Flight number</th>" + "<th>Airplane type</th>" + "<th>Departure city</th>"
				+ "<th>Departure time</th>" + "<th>Arrival city</th>" + "<th>Arrival time</th>" + "</tr>");
		for (Flight flight : flights) {
			try {
				out.print("<tr>");
				out.print("" + "<td>" + flight.getIdFlight() + "</td>" + "<td>" + flight.getAirplaneType() + "</td>"
						+ "<td>" + flight.getDepartureCity() + "</td>" + "<td>" + flight.getDepartureTime().toString()
						+ "</td>" + "<td>" + flight.getArrivalCity() + "</td>" + "<td>"
						+ flight.getArrivalTime().toString() + "</td>" + "");
				out.print("</tr>");
			} catch (Exception e) {

			}
		}
		out.print("</table>");
		out.println("<form action = \"LoginServlet\" method = \"GET\">"
				+ "<input type = \"submit\" value = \"Logout\" />" + "</form>");
		out.print("</body></html>");

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doGet(request, response);
	}
}

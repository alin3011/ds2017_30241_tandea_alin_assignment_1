package servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import access.FlightDAO;

/**
 * Servlet implementation class FlightHandleServlet
 */
public class FlightHandleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	FlightDAO f;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FlightHandleServlet() {
		super();
		f = new FlightDAO();
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Integer role = (Integer) session.getAttribute("role");
		if (role != null && role.intValue() == 1) {
			DateFormat lFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			Date departureTime = null;
			Date arrivalTime = null;
			String dTime = request.getParameter("departureTime");
			String aTime = request.getParameter("arrivalTime");
			String idFlight = request.getParameter("idFlight");
			String airplaneType = request.getParameter("airplaneType");
			String departureCity = request.getParameter("departureCity");
			try {
				departureTime = (Date) lFormatter.parse(dTime.replaceAll("Z$", "+0000"));
				arrivalTime = (Date) lFormatter.parse(aTime.replaceAll("Z$", "+0000"));
			} catch (Exception e) {

			}
			String arrivalCity = request.getParameter("arrivalCity");
			if (!idFlight.isEmpty()) {
				f.updateFlight(Integer.parseInt(idFlight), airplaneType, departureCity, departureTime, arrivalCity,
						arrivalTime);
			} else {
				f.addFlight(airplaneType, departureCity, departureTime, arrivalCity, arrivalTime);
			}
			response.sendRedirect("/flights/admin");
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idFlight = request.getParameter("idFlight");
		if (idFlight != null) {
			f.deleteFlight(Integer.parseInt(idFlight));
		}
		response.sendRedirect("/flights/admin");
	}

}

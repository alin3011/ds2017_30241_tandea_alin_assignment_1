package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import access.UserDAO;
import model.User;

// Extend HttpServlet class
public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 321071711291795779L;

	UserDAO u = new UserDAO();

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.removeAttribute("username");
		session.removeAttribute("role");
		response.sendRedirect("/flights/loginForm");
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		PrintWriter out = response.getWriter();
		User user = u.verifyLogIn(username, password);
		if (user == null) {
			out.print("Sorry, username or password error!");
		} else {
			HttpSession session = request.getSession();
			session.setAttribute("username", user.getUsername());
			int role = user.getRole();
			session.setAttribute("role", role);
			if (role == 0) {
				// request.getRequestDispatcher("/display").include(request, response);
				response.sendRedirect("/flights/display");
			} else if (role == 1) {
				// request.getRequestDispatcher("/admin").include(request, response);
				response.sendRedirect("/flights/admin");
			}
		}
	}
}
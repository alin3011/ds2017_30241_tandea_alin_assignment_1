package access;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import model.Flight;
import utils.HibernateUtil;

public class FlightDAO {

	private SessionFactory sessionFactory;

	/**
	 * 
	 */
	public FlightDAO() {
		super();
		this.sessionFactory = HibernateUtil.getSessionFactory();

	}

	public int addFlight(String airplaneType, String departureCity, Date departureTime, String arrivalCity,
			Date arrivalTime) {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Flight f = new Flight();
		f.setAirplaneType(airplaneType);
		f.setDepartureCity(departureCity);
		f.setDepartureTime(departureTime);
		f.setArrivalCity(arrivalCity);
		f.setArrivalTime(arrivalTime);
		Integer id = (Integer) session.save(f);
		tx.commit();
		session.close();
		return id.intValue();
	}

	public void deleteFlight(int idFlight) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Flight f = session.get(Flight.class, idFlight);
		session.delete(f);
		tx.commit();
		session.close();
	}

	public void updateFlight(int idFlight, String airplaneType, String departureCity, Date departureTime,
			String arrivalCity, Date arrivalTime) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Flight f = session.get(Flight.class, idFlight);
		f.setAirplaneType(airplaneType);
		f.setDepartureCity(departureCity);
		f.setDepartureTime(departureTime);
		f.setArrivalCity(arrivalCity);
		f.setArrivalTime(arrivalTime);
		session.update(f);
		tx.commit();
		session.close();
	}

	public List<Flight> findAll() {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Flight> flights = session.createQuery("select f from Flight f").list();
		tx.commit();
		session.close();
		return (ArrayList<Flight>) flights;
	}
	
	public Flight getFlight(int idFlight) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Criteria c = session.createCriteria(Flight.class);
		c.add(Restrictions.eq("idFlight", idFlight));
		List<Flight> results = c.list();
		if(!results.isEmpty()) {
			return results.get(0);
		}else {
			return null;
		}
	}
}

package access;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import model.User;
import utils.HibernateUtil;

public class UserDAO {

	private SessionFactory sessionFactory;

	public UserDAO() {
		super();
		this.sessionFactory = HibernateUtil.getSessionFactory();
	}

	public User verifyLogIn(String username, String password) {
		Query query = null;
		Session session = sessionFactory.openSession();
		try {
			query = session.createQuery("from User u where u.username=:username and u.password =:password");
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		query.setParameter("username", username);
		query.setParameter("password", password);
		@SuppressWarnings("unchecked")
		List<User> list = query.list();
		if (list.size() > 0) {
			session.close();
			return list.get(0);
		}
		session.close();
		return null;
	}

	public int addUser(String username, String password, int role) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		User u = new User();
		u.setUsername(username);
		u.setPassword(password);
		u.setRole(role);
		Integer i = (Integer) session.save(u);
		tx.commit();
		session.close();
		return i.intValue();
	}

}

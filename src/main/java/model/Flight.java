package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "flight")
public class Flight {
	@Id
	@GeneratedValue
	@Column(name = "idflight")
	private int idFlight;
	@Column(name = "airplaneType")
	private String airplaneType;
	@Column(name = "departureCity")
	private String departureCity;
	@Column(name = "departureTime")
	private Date departureTime;
	@Column(name = "arrivalCity")
	private String arrivalCity;
	@Column(name = "arrivalTime")
	private Date arrivalTime;

	/**
	 * @return the departureCity
	 */
	public String getDepartureCity() {
		return departureCity;
	}

	/**
	 * @param departureCity
	 *            the departureCity to set
	 */
	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}

	/**
	 * @return the departureTime
	 */
	public Date getDepartureTime() {
		return departureTime;
	}

	/**
	 * @param departureTime
	 *            the departureTime to set
	 */
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	/**
	 * @return the arrivalCity
	 */
	public String getArrivalCity() {
		return arrivalCity;
	}

	/**
	 * @param arrivalCity
	 *            the arrivalCity to set
	 */
	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	/**
	 * @return the arrivalTime
	 */
	public Date getArrivalTime() {
		return arrivalTime;
	}

	/**
	 * @param arrivalTime
	 *            the arrivalTime to set
	 */
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	/**
	 * @return the idFlight
	 */
	public int getIdFlight() {
		return idFlight;
	}

	/**
	 * @param idFlight
	 *            the idFlight to set
	 */
	public void setIdFlight(int idFlight) {
		this.idFlight = idFlight;
	}

	/**
	 * @return the airplaneType
	 */
	public String getAirplaneType() {
		return airplaneType;
	}

	/**
	 * @param airplaneType
	 *            the airplaneType to set
	 */
	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

}
